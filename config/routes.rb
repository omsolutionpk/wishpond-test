Rails.application.routes.draw do
  resources :plays, only: [:index, :create]
  resources :images, only: [:index, :create]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'images#index'
  
end
