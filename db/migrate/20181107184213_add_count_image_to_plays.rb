class AddCountImageToPlays < ActiveRecord::Migration[5.2]
  def change
    add_column :plays, :count_value, :integer
    add_column :plays, :img, :text
  end
end
