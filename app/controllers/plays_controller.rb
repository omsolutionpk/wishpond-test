class PlaysController < ApplicationController
  before_action :set_play, only: [:show, :edit, :update, :destroy]

  def index
    @plays  = Play.all
    @images = Image.random(10).map{|i|url_for(i)}
  end

  def create
    @play = Play.new(play_params)

    respond_to do |format|
      if @play.save
        format.html { redirect_to @play, notice: 'Play was successfully created.' }
        format.json { render json: {data:@play, html: (render_to_string :partial => 'plays/table_row.html.erb', :locals => {play:@play})},status: :created }
      else
        format.html { render :new }
        format.json { render json: @play.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    def set_play
      @play = Play.find(params[:id])
    end

    def play_params
      params.require(:play).permit(:img,:count_value)
    end
end
