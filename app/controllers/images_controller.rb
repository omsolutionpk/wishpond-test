class ImagesController < ApplicationController
  before_action :set_image, only: [:show, :edit, :update, :destroy]

  def index
    @images = Image.all
  end

  def create
    @image  = Image.new(image_params)
    
    if @image.save
      redirect_to images_path
    else
    end
  end

  private
    def set_image
      @image = Image.find(params[:id])
    end

    def image_params
      params.require(:image).permit(uploads: [])
    end
end
