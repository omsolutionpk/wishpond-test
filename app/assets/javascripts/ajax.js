function Ajax({url,method,data,contentType,onSuccessCallback, onErrorCallback}){
	let that = this;
	this.httpRequest = null,
	this.onSuccess   = onSuccessCallback,
	this.onError     = onErrorCallback,
	this.url         = url,
	this.method      = method,
	this.data        = data,
	this.token       = null,
	this.contentType = contentType || 'application/json',

	this.init = function(){
		this.httpRequest = new XMLHttpRequest();

	    if (!this.httpRequest) {
	      alert('Giving up :( Cannot create an XMLHTTP instance');
	      return false;
	    }

	    this.httpRequest.onreadystatechange = this.stateChangeCallback;
		this.token = document.querySelector('[name="authenticity_token"]').value;

	},

	this.makeRequest = function(){
		this.httpRequest.open(this.method, this.url);
	    this.httpRequest.setRequestHeader('Content-Type', this.contentType);
	  	this.httpRequest.setRequestHeader('Accept', this.contentType);
	  	this.httpRequest.setRequestHeader('X-CSRF-Token',this.token);
	  	
	  	if(this.contentType === 'application/json'){
	  		this.data = JSON.stringify(data);
	  	}

	    this.httpRequest.send(this.data);
	},

	this.stateChangeCallback = function(){
		if (that.httpRequest.readyState === XMLHttpRequest.DONE) {
			let response = JSON.parse(that.httpRequest.responseText);

			if (that.httpRequest.status < 300) {
				that.onSuccess(response);
			} else {
				console.log(that.url);
				that.onError(response);
			}
	    }
	},

	this.init();
}


