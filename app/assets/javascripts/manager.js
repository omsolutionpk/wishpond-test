function Manager() {
	let that            = this;
	const BTN_ID        = "capture";
	const IMAGES_ID     = "images-urls";
	const TARGET_IMG_ID = "target-img";

	this.counter       = null,
	this.btnCapture    = null,
	this.targetImg     = null,
	this.imagesArr     = [],

	this.init = function(){
		this.imagesArr  = document.getElementById(IMAGES_ID).dataset.urls.split('|');
		this.btnCapture = document.getElementById(BTN_ID);
		this.targetImg  = document.getElementById(TARGET_IMG_ID);

		this.counter   = new Counter({onCountChangeCallback: function(tickValue){
			let imgUrl    = that.pickImage(tickValue);
			that.updateImage(imgUrl)
		}});
		
		this.counter.start();
	},

	this.pickImage = function(tick){
		let diff     = (this.counter.counterStart() - tick);
		let quotient = parseInt(diff/this.imagesArr.length);
		let index    = diff - (quotient * this.imagesArr.length);
		
		console.log( tick +' =' + index);
		return this.imagesArr[index];
	},

	this.btnPressed = function(e){
		e.preventDefault();
		let tickValue = that.counter.count;
    	let imgUrl    = that.pickImage(tickValue);

		let ajax = new Ajax({
				url:'/plays',
				method:'POST',
				data:{play:{count_value:tickValue, img:imgUrl}}, 
				onSuccessCallback: that.updateTable, 
				onErrorCallback:function(error){
					alert('some error occurred while saving to database');
				}
			});

    	ajax.makeRequest();
		that.updateImage(imgUrl);

	},

	this.updateTable = function(data){
		var tableRef = document.getElementById('table-listing').getElementsByTagName('tbody')[0];
		var row   = tableRef.insertRow(tableRef.rows.length);
		row.innerHTML = data.html;
	},

	this.updateImage = function(imgUrl){
		if(imgUrl){
			that.targetImg.src = imgUrl;
			that.targetImg.style.display = 'inline';
		}
	},

	this.init();
	this.btnCapture.addEventListener("click", this.btnPressed);

}