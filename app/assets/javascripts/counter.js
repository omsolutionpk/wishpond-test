function Counter({onCountChangeCallback}) {
	let that = this;

	const RESOLUTION    = "1000"; // in milliseconds
	const COUNTER_START = "10";
	const TARGET_ID     = "counter";
	

	this.count         = null,
	this.target        = null,
	this.resolution    = null, 

	this.onCountChangeCallback = onCountChangeCallback,


	this.init = function(){
		this.count         = COUNTER_START;
		this.resolution    = RESOLUTION;
		this.target        = document.getElementById(TARGET_ID);
	},

	this.start = function(){
		setInterval(function(){ that.render() }, this.resolution);
	},

	this.updateCount = function(){
		this.count--;

		if(this.count <= 0){
			this.count = COUNTER_START;
		}
	},

	this.render = function(){
    	this.updateCount();

		if(this.target){
    		this.target.innerHTML = this.count;
    	}

    	if(this.onCountChangeCallback){
    		this.onCountChangeCallback(this.count);
    	}
	},

	this.counterStart = function(){
		return COUNTER_START;
	},

	this.init();
}


