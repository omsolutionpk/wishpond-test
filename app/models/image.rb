class Image < ApplicationRecord
	has_many_attached :uploads
	scope :random, ->(limit){ActiveStorage::Attachment.order("RANDOM()").limit(limit)}
end
