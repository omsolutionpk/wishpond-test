class Play < ApplicationRecord
	validates :count_value, presence: true
	validates :img, presence: true
end
