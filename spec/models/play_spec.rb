require 'rails_helper'

RSpec.describe Play, type: :model do
	it { should validate_presence_of(:img) }
	it { should validate_presence_of(:count_value) }
end
